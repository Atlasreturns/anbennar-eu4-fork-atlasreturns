building_ruin_cliff_passage = {
	local_state_maintenance_modifier = 1
	province_trade_power_value = 5
	#picture = "ruin_cliff_passage_grayed_out"
}

ruin_cliff_passage = {
	local_state_maintenance_modifier = 0.5
	province_trade_power_value = 10
	local_defensiveness = 0.2
	#picture = "ruin_cliff_passage"
}

cancelled_ruin_cliff_passage = {
}

ruin_cliff_passage_link = {
	province_trade_power_value = 5
	#picture = "ruin_cliff_passage"
}

m_redrushes_climb = {
	local_state_maintenance_modifier = 0.5
	province_trade_power_value = 10
	local_defensiveness = 0.2
	#picture = "ruin_cliff_passage"
}

m_spoorland_lift = {
	local_state_maintenance_modifier = 0.5
	province_trade_power_value = 10
	local_defensiveness = 0.2
	#picture = "ruin_cliff_passage"
}

m_walkway_of_thorns = {
	local_state_maintenance_modifier = 0.5
	province_trade_power_value = 10
	local_defensiveness = 0.2
	#picture = "ruin_cliff_passage"
}

m_arca_noruin = {
	local_state_maintenance_modifier = 0.5
	province_trade_power_value = 10
	local_defensiveness = 0.2
	#picture = "ruin_cliff_passage"
}

m_arca_venaan = {
	local_state_maintenance_modifier = 0.5
	province_trade_power_value = 10
	local_defensiveness = 0.2
	#picture = "ruin_cliff_passage"
}

m_arbeloch_ascensor = {
	local_state_maintenance_modifier = 0.5
	province_trade_power_value = 10
	local_defensiveness = 0.2
	#picture = "ruin_cliff_passage"
}