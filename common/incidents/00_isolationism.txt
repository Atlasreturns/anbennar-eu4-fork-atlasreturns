###################
##NSC Incidents
###################


incident_placeholder = {

	frame = 1
	variable_initial = 0

	potential = {
		has_dlc = "Mandate of Heaven"
		religion = bulwari_sun_cult
	}

	trigger = {
		always = no
	}

	mean_time_to_happen = {
		months = 1
	}

	immediate_effect = {
		set_country_flag = active_incident_flag
	}
}

incident_placeholder = {

	frame = 2
	variable_initial = 0

	potential = {
		has_dlc = "Mandate of Heaven"
		religion = bulwari_sun_cult
	}

	trigger = {
		always = no
	}

	mean_time_to_happen = {
		months = 1
	}

	immediate_effect = {
		set_country_flag = active_incident_flag
	}
}

incident_placeholder = {

	frame = 3
	variable_initial = 0

	potential = {
		has_dlc = "Mandate of Heaven"
		religion = bulwari_sun_cult
	}

	trigger = {
		always = no
	}

	mean_time_to_happen = {
		months = 1
	}

	immediate_effect = {
		set_country_flag = active_incident_flag
	}
}

incident_placeholder = {

	frame = 4
	variable_initial = 0

	potential = {
		has_dlc = "Mandate of Heaven"
		religion = bulwari_sun_cult
	}

	trigger = {
		always = no
	}

	mean_time_to_happen = {
		months = 1
	}

	immediate_effect = {
		set_country_flag = active_incident_flag
	}
}

incident_placeholder = {

	frame = 5
	variable_initial = 0

	potential = {
		has_dlc = "Mandate of Heaven"
		religion = bulwari_sun_cult
	}

	trigger = {
		always = no
	}

	mean_time_to_happen = {
		months = 1
	}

	immediate_effect = {
		set_country_flag = active_incident_flag
	}
}

incident_placeholder = {

	frame = 6
	variable_initial = 0

	potential = {
		has_dlc = "Mandate of Heaven"
		religion = bulwari_sun_cult
	}

	trigger = {
		always = no
	}

	mean_time_to_happen = {
		months = 1
	}

	immediate_effect = {
		set_country_flag = active_incident_flag
	}
}

incident_placeholder = {

	frame = 7
	variable_initial = 0

	potential = {
		has_dlc = "Mandate of Heaven"
		religion = bulwari_sun_cult
	}

	trigger = {
		always = no
	}

	mean_time_to_happen = {
		months = 1
	}

	immediate_effect = {
		set_country_flag = active_incident_flag
	}
}

incident_placeholder = {

	frame = 8
	variable_initial = 0

	potential = {
		has_dlc = "Mandate of Heaven"
		religion = bulwari_sun_cult
	}

	trigger = {
		always = no
	}

	mean_time_to_happen = {
		months = 1
	}

	immediate_effect = {
		set_country_flag = active_incident_flag
	}
}

incident_placeholder = {

	frame = 9
	variable_initial = 0

	potential = {
		has_dlc = "Mandate of Heaven"
		religion = bulwari_sun_cult
	}

	trigger = {
		always = no
	}

	mean_time_to_happen = {
		months = 1
	}

	immediate_effect = {
		set_country_flag = active_incident_flag
	}
}

incident_placeholder = {

	frame = 10
	variable_initial = 0

	potential = {
		has_dlc = "Mandate of Heaven"
		religion = bulwari_sun_cult
	}

	trigger = {
		always = no
	}

	mean_time_to_happen = {
		months = 1
	}

	immediate_effect = {
		set_country_flag = active_incident_flag
	}
}

incident_placeholder = {

	frame = 11
	variable_initial = 0

	potential = {
		has_dlc = "Mandate of Heaven"
		religion = bulwari_sun_cult
		always = no
	}

	trigger = {
		always = no
	}

	mean_time_to_happen = {
		months = 1
	}

	immediate_effect = {
		set_country_flag = active_incident_flag
	}
}

incident_placeholder = {

	frame = 12
	variable_initial = 0

	potential = {
		has_dlc = "Mandate of Heaven"
		religion = bulwari_sun_cult
		always = no
	}

	trigger = {
		always = no
	}

	mean_time_to_happen = {
		months = 1
	}

	immediate_effect = {
		set_country_flag = active_incident_flag
	}
}

incident_placeholder = {

	frame = 13
	variable_initial = 0

	potential = {
		has_dlc = "Mandate of Heaven"
		religion = bulwari_sun_cult
		always = no
	}

	trigger = {
		always = no
	}

	mean_time_to_happen = {
		months = 1
	}

	immediate_effect = {
		set_country_flag = active_incident_flag
	}
}

incident_placeholder = {

	frame = 14
	variable_initial = 0

	potential = {
		has_dlc = "Mandate of Heaven"
		religion = bulwari_sun_cult
		always = no
	}

	trigger = {
		always = no
	}

	mean_time_to_happen = {
		months = 1
	}

	immediate_effect = {
		set_country_flag = active_incident_flag
	}
}

incident_placeholder = {

	frame = 15
	variable_initial = 0

	potential = {
		has_dlc = "Mandate of Heaven"
		religion = bulwari_sun_cult
		always = no
	}

	trigger = {
		always = no
	}

	mean_time_to_happen = {
		months = 1
	}

	immediate_effect = {
		set_country_flag = active_incident_flag
	}
}

###################
##End of NSC Incidents
###################